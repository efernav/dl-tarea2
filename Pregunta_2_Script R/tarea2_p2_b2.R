# Aprendizaje Profundo
# Tarea 2, p2
# Alumno: Enrique Fernández Vicente

library(tensorflow)
library(keras)
library(readr)
library(caret)

# 1. Definición de parametros del modelo
caso <- "data augmention, 800 steps per epochs"
pregunta <- "2-cifar10"
letra <- "b2"

batch_size <- 50
epochs <- 100

# 2. Carga de los datos
# Utilizamos el dataset de keras
cifar10 <- dataset_cifar10()

# Separamos los grupos de datos
c(c(x_train, y_train), c(x_test, y_test)) %<-% cifar10

# 3. Preparaciòn de los datos
# Necesitamos escalar los valores entre 0 y 1
x_train <- x_train/255
x_test <- x_test/255

# Transformar a categorias los tipos de imagenes
y_train <- to_categorical(y_train, num_classes = 10)
y_test <- to_categorical(y_test, num_classes = 10)
class_labels <- c("airplanes", "cars", "birds", "cats", "deer", "dogs", 
                  "frogs", "horses", "ships", "trucks")

#Se toma un subconjunto de 10.000 de los datos de entrenamiento (40.000) que serán utilizados para validar
val_indices <- 1:10000
x_val <- x_train[val_indices,,,]
partial_x_train <- x_train[-val_indices,,,]
y_val <- y_train[val_indices,]
partial_y_train <- y_train[-val_indices,]

dim (x_train)
dim (y_train)
dim (x_test)
dim (y_test)
dim (partial_x_train)
dim (partial_y_train)
dim (x_val)
dim (y_val)

# 4. Definición del modelo
model <- keras_model_sequential()

model %>%
  layer_conv_2d(filter = 32, kernel_size = c(4,4), padding = "same", input_shape = c(32, 32, 3)) %>%
  layer_activation("relu") %>%

  layer_conv_2d(filter = 32, kernel_size = c(4,4)) %>%
  layer_activation("relu") %>%

  layer_max_pooling_2d(pool_size = c(2,2)) %>%
  
  layer_conv_2d(filter = 32, kernel_size = c(4,4), padding = "same") %>%
  layer_activation("relu") %>%

  layer_max_pooling_2d(pool_size = c(2,2)) %>%  
    
  layer_conv_2d(filter = 32, kernel_size = c(4,4)) %>%
  layer_activation("relu") %>%
  
  layer_max_pooling_2d(pool_size = c(2,2)) %>%

  layer_flatten() %>%
  layer_dense(512) %>%
  layer_activation("relu") %>%
  
  layer_dense(10) %>%
  layer_activation("softmax")

summary(model)

opt <- optimizer_rmsprop ()

model %>% compile(
  loss = "categorical_crossentropy",
  optimizer = opt,
  metrics = "accuracy"
)

# 5. Entrenamiento
datagen <- image_data_generator(
  rotation_range = 40, 
  width_shift_range = 0.2, 
  height_shift_range = 0.2, 
  shear_range = 0.2, 
  zoom_range = 0.2,
  horizontal_flip = TRUE,
  fill_mode = "nearest"
)

datagen %>% fit_image_data_generator(partial_x_train)

history <- model %>% fit_generator(
  flow_images_from_data(partial_x_train, partial_y_train, datagen, batch_size = batch_size),
  steps_per_epoch = as.integer(40000/batch_size), 
  epochs = epochs, 
  validation_data = list(x_test, y_test)
)


print (history)

png(paste(pregunta,".letra-",letra,".epocas-",epochs,"caso ",caso,".png"))

grafico <- plot (history)

print (grafico)

dev.off() 

model %>% save_model_hdf5 (paste(pregunta,".letra-",letra,".epocas-",epochs,"caso ",caso,".hdf5"))
weights <- get_weights(model)

save(weights,   file=paste(pregunta,".letra-",letra,".epocas-",epochs,"caso ",caso,".rda"))

# 6. Validación

pred_prob <- predict(object = model, x = x_val)

y_pred_class_label <- rep("", dim(y_val)[1]); str(y_pred_class_label)

dim(y_test)
head(y_test)

# Por cada uno de las 10.000 imagenes de test se busca en la predicción con la probabilidad mayor dentro de la 10 clases 
for (i in 1:dim(y_test)[1]) {   
  for (j in 1:dim(y_test)[2]) {
    if(j==1) j_max = 1
    else if (pred_prob[i,j] > pred_prob[i, j_max]) j_max = j
  }
  # Se busca la etiqueta de la categorìa para la que tuvo mayor probabilidad en la predicción
  y_pred_class_label[i] <- class_labels[j_max]
}

y_test_class_label <- rep("", dim(y_test)[1]); str(y_test_class_label)

# Por cada uno de las 10.000 imagenes de test se asocia la etiqueta del nombre de la catgoria
for (i in 1:dim(y_test)[1]) {   # dim(y_train) is 10000(images) * 10 (class-label-indicators)
  for (j in 1:dim(y_test)[2]) {
    if (y_val[i,j] == 1) 
      y_test_class_label[i] <- class_labels[j]
  }
}

length(y_test_class_label)==length(y_pred_class_label)

confusionMatrix(as.factor(y_pred_class_label), as.factor(y_test_class_label))


